package no.app.test.taskmob;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by froseti on 02.05.16.
 *
 * Работае с конкретным продуктом и его отзывами
 */
public class ProductActivity extends AppCompatActivity {
    public static final String INDEX = "index";
    public static final String TAG = "ProductActivity";

    private ImageView mImgView;
    private Button mBtnReview, mBtnSendReview;
    private TextView mTvRate, mTvText, mTvCountRev;
    private Product mProduct;
    private static ArrayList<Review> mReviews;
    private ListView mList;
    private RelativeLayout mRlReview, mRlInfo, mRlSetReview;
    private MenuItem mMenuItemitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        //получаем индекс текущего продукта
        int index = getIntent().getIntExtra(INDEX, -1);
        if (index < 0) {
            Toast.makeText(this, getText(R.string.error), Toast.LENGTH_SHORT).show();
            return;
        }
        mProduct = MainActivity.getProducts().get(index);

        mImgView = (ImageView) findViewById(R.id.iv_pic);
        mTvRate = (TextView) findViewById(R.id.tv_rate);
        mTvText = (TextView) findViewById(R.id.tv_text_prod);
        mTvCountRev = (TextView) findViewById(R.id.tv_count_reviews);
        mBtnReview = (Button) findViewById(R.id.btn_review);
        mBtnSendReview = (Button) findViewById(R.id.btn_set_rew);
        mList = (ListView) findViewById(R.id.list_reviews);
        mRlReview = (RelativeLayout) findViewById(R.id.rl_review);
        mRlInfo = (RelativeLayout) findViewById(R.id.rl_info);
        mRlSetReview = (RelativeLayout) findViewById(R.id.rl_set_review);

        //Запускаем новый поток, для получения списка отзывов о данном продукте
        new AsyncOperation(this).execute(WorkWithServer.GET_REVIEWS, String.valueOf(mProduct.getId()));


        mImgView.setImageBitmap(MainActivity.getBitmap().get(index));
        mTvText.setText(mProduct.getText().trim());


        /* Т.к. эта активность содержит информацию о продукте, список отзывов и
        форму для отправки своего отзыва (такое себе решение), то очень часто приходится
        использовать свойства видимости практически всех элеменотов активности.
         */

        mBtnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mRlReview.getVisibility() == View.VISIBLE) {
                    mRlReview.setVisibility(View.GONE);
                    mRlInfo.setVisibility(View.VISIBLE);
                    mBtnReview.setText("Отзывы");
                    mBtnReview.setVisibility(View.VISIBLE);
                    mTvCountRev.setVisibility(View.VISIBLE);
                    mMenuItemitem.setVisible(false);
                    mRlSetReview.setVisibility(View.GONE);
                    mList.setVisibility(View.VISIBLE);
                } else {
                    mRlReview.setVisibility(View.VISIBLE);
                    mRlInfo.setVisibility(View.GONE);
                    mBtnReview.setText("Описание товара");
                    mTvCountRev.setVisibility(View.GONE);

                    //Если пользователь в системе, то отображаем в меню "Оставить отзыв"
                    if (User.getInstance().isInit())
                        mMenuItemitem.setVisible(true);
                }
            }
        });
        mBtnSendReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RatingBar rate = (RatingBar) findViewById(R.id.ratingBar);
                EditText text = (EditText) findViewById(R.id.et_review);

                //Второй поток, для отправки отзыва на сервер

                new AsyncOperation(ProductActivity.this)
                        .execute(WorkWithServer.POST_REVIEW,
                                String.valueOf((int) rate.getRating()),
                                text.getText().toString().trim(),
                                String.valueOf(mProduct.getId()));

                mRlSetReview.setVisibility(View.GONE);
                mList.setVisibility(View.VISIBLE);
                mBtnReview.setVisibility(View.VISIBLE);
            }
        });

    }

    /*Немного переназначаем кнопку Back, т.к. если этого не сделать, то при просмотре
    отзывов или находясь на форме отправки отзыва, мы вылетим на главный экран, что
    было бы достаточно странно.
    */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mRlInfo.getVisibility() == View.GONE) {
            mBtnReview.callOnClick();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_menu, menu);
        mMenuItemitem = menu.findItem(R.id.menu_review);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_review:
                mRlSetReview.setVisibility(View.VISIBLE);
                mList.setVisibility(View.GONE);
                mBtnReview.setVisibility(View.GONE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static ArrayList<Review> getReviews() {
        return mReviews;
    }

    public static void setReviews(ArrayList<Review> mReviews) {
        ProductActivity.mReviews = mReviews;
    }

    private float getRate() {
        float res = 0;
        Log.d(TAG, mReviews.size() + "");
        for (Review r : mReviews) {
            res += r.getRate();
        }
        if (mReviews.size() != 0) res = res / mReviews.size();
        return res;
    }

    /**
     * Инициализирует необходимые поля, когда второй поток получит с сервера список отзывов.
     */
    public void init() {
        String rate = String.format("%.1f", getRate());
        mTvRate.setText(getText(R.string.rate) + rate);
        mTvCountRev.setText("(" + mReviews.size() + ")");
        mList.setAdapter(new ReviewListAdapter(this, mReviews));
        mBtnReview.setEnabled(true);
    }

    /**
     * Адаптер для списка отзывов
     */
    private class ReviewListAdapter extends ArrayAdapter<Review> {

        public ReviewListAdapter(Activity activity, List<Review> reviews) {
            super(activity, 0, reviews);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // Если мы не получили представление, заполняем его
            if (convertView == null) {
                convertView = ProductActivity.this.getLayoutInflater()
                        .inflate(R.layout.item_for_list_review, null);
            }
            // Настройка представления для объекта Review
            Review r = getItem(position);
            ((TextView) convertView.findViewById(R.id.tv_user_review)).setText(r.getUserName());
            ((TextView) convertView.findViewById(R.id.tv_text_review)).setText(r.getText());
            ((TextView) convertView.findViewById(R.id.tv_rate_reviews)).setText("Оценка: " + r.getRate());
            return convertView;
        }
    }
}
