package no.app.test.taskmob;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
/**
 * Created by froseti on 02.05.16.
 *
 * Модель пользователя. Сиглетный класс, т.к. приложением может пользоваться в опредиленный
 * момент только один пользователь. И доступ к данным пользователя может понадобиться с
 * любой точки приложения.
 */
public class User {
    public static final String TAG = "User";

    private static User instance;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String USERNAME = "username";
    public static final String PASS = "pass";
    public static final String TOKEN = "token";
    public static final String ID = "id";

    private int mId;
    private String mUsername;
    private String mPassword;
    private String mToken;

    private boolean isInit = false; // флаг, о полной инициализации пользователя
    SharedPreferences mSharedPreferences;

    public static User getInstance() {
        if (instance == null) instance = new User();
        return instance;
    }

    private User() {
    }

    /**
     * Инициализирует пользователя если надо.
     *
     * @param id
     * @param username
     * @param pass
     * @param token
     */
    public void init(int id, String username, String pass, String token) {
        mId = id;
        mUsername = username;
        mPassword = pass;
        mToken = token;
        isInit = true;
    }

    /**
     * Стирает все данные о пользователе, включая и те которые хранили в Preferences
     *
     * @param context
     */
    public void clearUser(Context context) {
        mId = 0;
        mUsername = mPassword = mToken = "";
        isInit = false;
        mSharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(ID, 0);
        editor.putString(USERNAME, "");
        editor.putString(PASS, "");
        editor.putString(TOKEN, "");
        editor.commit();
    }

    public int getId() {
        return mId;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getToken() {
        return mToken;
    }

    public boolean isInit() {
        return isInit;
    }

    /**
     * Сохраняем данные о пользователе в Preferences
     *
     * @param context
     * @return
     */
    public boolean saveData(Context context) {
        mSharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(ID, mId);
        editor.putString(USERNAME, mUsername);
        editor.putString(PASS, mPassword);
        editor.putString(TOKEN, mToken);
        editor.commit();
        return true;
    }

    /**
     * Инициализируем пользователя, ранее сохраненными данными (если они есть)
     *
     * @param context
     * @return
     */
    public boolean getData(Context context) {
        mSharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mId = mSharedPreferences.getInt(ID, 0);
        mUsername = mSharedPreferences.getString(USERNAME, "");
        mPassword = mSharedPreferences.getString(PASS, "");
        mToken = mSharedPreferences.getString(TOKEN, "");
        isInit = TextUtils.isEmpty(mToken) ? false : true;
        return isInit;
    }
}
