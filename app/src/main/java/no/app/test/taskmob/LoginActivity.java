package no.app.test.taskmob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
/**
 * Created by froseti on 03.05.16.
 *
 * Обработка регистрационной активности (вход или регистрация)
 */
public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LoginActivity";
    public static final String KIND = "kind"; //используем для получения из интента (запустившего
                                               // эту активность) инструкции что будем делать,
                                               // просто вход в систему или регистрация
    public static final int OK = 1;
    public static final int ERR = -1;

    String mUsername, mPass, mKind;
    Button mBtn;
    EditText mPassw;
    AutoCompleteTextView mName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mKind = getIntent().getStringExtra(KIND);

        mBtn = (Button)findViewById(R.id.btn_sign);
        mName = (AutoCompleteTextView)findViewById(R.id.tv_name);
        mPassw = (EditText)findViewById(R.id.password);

        if(mKind.equals(WorkWithServer.REGISTRATION))
            mBtn.setText(getText(R.string.registration));
        else mBtn.setText(getText(R.string.enter));

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUsername = mName.getText().toString();
                mPass = mPassw.getText().toString();

                //запускаем еще один поток, для работы с сервером
               new AsyncOperation(LoginActivity.this).execute(mKind, mUsername, mPass);
            }
        });
    }
}
