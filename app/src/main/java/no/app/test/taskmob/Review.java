package no.app.test.taskmob;
/**
 * Created by froseti on 02.05.16.
 *
 * Модель отзыва (получаемого с сервера)
 */

public class Review {
    private int mId;
    private int mRate;
    private String mText;
    private int mIdUser;
    private int mIdProduct;
    private String mUserName;

    public Review(){}

    public Review(int id, int rate, String text, int idUser, int idProduct) {
        mId = id;
        mRate = rate;
        mText = text;
        mIdUser = idUser;
        mIdProduct = idProduct;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getRate() {
        return mRate;
    }

    public void setRate(int rate) {
        mRate = rate;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public void setIdUser(int idUser) {
        mIdUser = idUser;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public void setIdProduct(int idProduct) {
        mIdProduct = idProduct;
    }
}
