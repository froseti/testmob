package no.app.test.taskmob;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by froseti on 02.05.16.
 *
 * Непосредственно работает с сервером
 */
public class WorkWithServer {

    //Необходимые для наших целей константы
    public static final String TAG = "WorkWithServer";
    public static final String URL_SERVER = "http://smktesting.herokuapp.com";
    public static final String URL_IMAGES = "http://smktesting.herokuapp.com/static/";
    public static final String REGISTRATION = "registration";
    public static final String GET_PRODUCTS = "get_products";
    public static final String POST_REVIEW = "post_review";
    public static final String GET_REVIEWS = "get_reviews";
    public static final String GET_TOKEN = "get_token";
    public static final String INET_OFF = "inet_off";
    public static final String ERR_CON = "ERR_CON";


    /**
     * Проверяем включенность передачи нанных на девайсе
     *
     * @param c
     * @return
     */
    public static boolean isOnline(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        if (nInfo != null) {
            Log.d(TAG, "ONLINE");
            return true;
        } else {
            Log.d(TAG, "OFFLINE");
            return false;
        }
    }

    /**
     * Отправляем POST-запросы на сервер, в зависимости от входных параметров
     *
     * @param params
     * @return
     */
    public static String methodPOST(String... params) {
        String POST_PARAMS = "";
        String str_url = "";
        String result = "";

        //В зависимости от параметра, конфигурируем JSON-запрос
        if (params[0].equals(WorkWithServer.REGISTRATION)) {
            str_url = URL_SERVER + "/api/register/";
            POST_PARAMS = "{\"username\":\"" + params[1] + "\"," +
                    "\"password\":\"" + params[2] + "\"}";
        }
        if (params[0].equals(WorkWithServer.GET_TOKEN)) {
            str_url = URL_SERVER + "/api/login/";
            POST_PARAMS = "{\"username\":\"" + params[1] + "\"," +
                    "\"password\":\"" + params[2] + "\"}";
        }
        if (params[0].equals(WorkWithServer.POST_REVIEW)) {
            str_url = URL_SERVER + "/api/reviews/" + params[3];
            POST_PARAMS = "{\"rate\":" + params[1] + "," +
                    "\"text\":\"" + params[2] + "\"}";
        }
        Log.d(TAG, "url = " + str_url);
        Log.d(TAG, "POST_PARAMS = " + POST_PARAMS);
        try {
            URL url = new URL(str_url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", String.valueOf(POST_PARAMS.length()));

            //Если есть возможность, отправляем серверу, в http заголовке, токен пользователя
            if (User.getInstance().isInit())
                connection.setRequestProperty("Authorization", "Token " + User.getInstance().getToken());
            // Записуем JSON в запрос
            OutputStream os = connection.getOutputStream();
            os.write(POST_PARAMS.getBytes());

            int responseCode = connection.getResponseCode();
            Log.d(TAG, "POST Response Code :: " + responseCode);

            //Если всё ОК, читаем и возвращаем результат от сервера.
            if (responseCode == HttpURLConnection.HTTP_CREATED ||
                    responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                result = response.toString();
            } else {
                Log.d(TAG, "POST запрос, не работает :(");
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
        return result;
    }

    /**
     * Отправляем GET-запросы на сервер, в зависимости от входных параметров
     *
     * @param params
     * @return
     * @throws IOException
     */

    public static String methodGET(String... params) {
        String result = "", str_url = "";

        if (params[0].equals(WorkWithServer.GET_PRODUCTS))
            str_url = URL_SERVER + "/api/products/";
        else if (params[0].equals(WorkWithServer.GET_REVIEWS))
            str_url = URL_SERVER + "/api/reviews/" + params[1];

        try {
            URL url = new URL(str_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Android");

            //Выполняем запрос и проверяем ответ и обрабатываем его
            int responseCode = con.getResponseCode();

            Log.d(TAG, "GET Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                result = response.toString();
            } else {
                Log.d(TAG, "GET запрос, не работает :(");
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
        return result;
    }
}