package no.app.test.taskmob;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
/**
 * Created by froseti on 02.05.16.
 *
 * Активити главного экрана
 */

public class MainActivity extends AppCompatActivity {

    private static ArrayList<Product> mProducts;
    private static ArrayList<Bitmap> mBitmap;
    private ListView mList;
    User mUser;
    TextView mUserName;
    MenuItem mMenuReg, mMenuEnter, mMenuOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUser = User.getInstance(); //получаем экземпляр пользователя (должен быть один)

        //Запускаем второй поток, для получения с сервера "продуктов"

        new AsyncOperation(MainActivity.this).execute(WorkWithServer.GET_PRODUCTS);

        mList = (ListView) findViewById(R.id.list);
        mUserName = (TextView) findViewById(R.id.tv_username);

        //Инициализируем пользователя, если данные о нем, ранее были сохранены (предпочтения)
        if (!mUser.isInit()) mUser.getData(this);

        //Если инициализация пользователя прошла успешно, то проверяем - помнит ли он нас.
        if (mUser.isInit()) {
            mUserName.setText(getString(R.string.you_enter_as) + mUser.getUsername());
            mUserName.setVisibility(View.VISIBLE);

            //Новый поток: подтверждаем у сервера токен пользователя (или нет, если сервер его удалил)
            new AsyncOperation(MainActivity.this).execute(WorkWithServer.GET_TOKEN, mUser.getUsername(), mUser.getPassword());
        }

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                //говорим с каким продуктом из коллекции следует работать
                intent.putExtra(ProductActivity.INDEX, i);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        mMenuReg = menu.findItem(R.id.menu_reg);
        mMenuEnter = menu.findItem(R.id.menu_enter);
        mMenuOut = menu.findItem(R.id.menu_out);
        // В зависимости от ситуации, отображаем или скрываем те или иные пункты меню
        initMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        // Операции для выбранного пункта меню
        switch (id) {
            case R.id.menu_enter:
                intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra(LoginActivity.KIND, WorkWithServer.GET_TOKEN);
                startActivityForResult(intent, 1);
                return true;
            case R.id.menu_out:
                User.getInstance().clearUser(MainActivity.this);
                initMenu();
                mUserName.setVisibility(View.GONE);
                return true;
            case R.id.menu_reg:
                intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra(LoginActivity.KIND, WorkWithServer.REGISTRATION);
                startActivityForResult(intent, 2);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Обрабатываем результат полученый от активити логина
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
            case 2:
                if (resultCode == LoginActivity.OK) {
                    mUserName.setText("Вы вошли, как " + User.getInstance().getUsername());
                    mUserName.setVisibility(View.VISIBLE);
                    initMenu();
                }
        }
    }

    public static ArrayList<Product> getProducts() {
        return mProducts;
    }

    public static void setProducts(ArrayList<Product> products) {
        MainActivity.mProducts = products;
    }

    public static ArrayList<Bitmap> getBitmap() {
        return mBitmap;
    }

    public static void setBitmap(ArrayList<Bitmap> bitmaps) {
        MainActivity.mBitmap = bitmaps;
    }

    public ListView getList() {
        return mList;
    }

    /**
     * В зависимости от ситуации, отображаем или скрываем те или иные пункты меню
     */
    public void initMenu() {
        if (User.getInstance().isInit()) {
            mMenuReg.setVisible(false);
            mMenuEnter.setVisible(false);
            mMenuOut.setVisible(true);
            mUserName.setText("Вы вошли, как " + User.getInstance().getUsername());
            mUserName.setVisibility(View.VISIBLE);
        } else {
            mMenuReg.setVisible(true);
            mMenuEnter.setVisible(true);
            mMenuOut.setVisible(false);
            mUserName.setVisibility(View.GONE);
        }
    }
}
