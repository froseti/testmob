package no.app.test.taskmob;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by froseti on 04.05.16.
 *
 * Используется для загрузки картинке по ее url и установке в элемент ImageView
 */
public class DownLoadImageTask extends AsyncTask<Void,Void,Bitmap> {
    public final String TAG = "DownLoadImageTask";
    String mUrlOfImage;
    ImageView mImageView;

    public DownLoadImageTask(ImageView imageView, String urlOfImage) {
        mUrlOfImage = urlOfImage;
        mImageView = imageView;
    }

    protected Bitmap doInBackground(Void... params) {

        Bitmap logo = null;
        try {
            InputStream is = new URL(mUrlOfImage).openStream();
            logo = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return logo;
    }

    protected void onPostExecute(Bitmap result) {
        mImageView.setImageBitmap(result);
    }
}