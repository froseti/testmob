package no.app.test.taskmob;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by froseti on 02.05.16.
 *
 * Используется для работы в отдельном потоке, в основном для общения с сервером.
 */
public class AsyncOperation extends AsyncTask<String, String, String> {

    public static final String TAG = "AsyncOperation";
    Activity mActivity; //Активность запустившая поток
    String mJson;
    JsonObject mJobj;
    boolean isSuccess = false; //Результат общения с сервером
    private ArrayList<Review> mReviews;
    private ProgressDialog mProgressDialog;

    public AsyncOperation(Activity activity){
        mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        showProgress("Loading...", mActivity);
        super.onPreExecute();
    }

    // Метод выполняется в новом потоке
    @Override
    protected String doInBackground(String... params) {
        try {

            if (!WorkWithServer.isOnline(mActivity)) {
                return WorkWithServer.INET_OFF;
            }

            /*Обрабатываем запросы полученные из запустившей поток активности
                используем классы WorkWithServer - для непосредственного общения с сервером и
                класс стороней библиотеки Gson - для работы с JSON ответами с сервера */

            switch (params[0]) {
                case WorkWithServer.REGISTRATION:
                    mJson = WorkWithServer.methodPOST(params[0], params[1], params[2]);
                    if (TextUtils.isEmpty(mJson)) {
                        isSuccess = false;
                        return WorkWithServer.ERR_CON;
                    }
                    mJobj = new Gson().fromJson(mJson, JsonObject.class);

                    isSuccess = mJobj.get("success").getAsBoolean();
                    if (isSuccess) {
                        User user = User.getInstance();
                        String token = mJobj.get("token").getAsString();
                        user.init(1, params[1], params[2], token);  //инициализируем пользователя
                        user.saveData(mActivity);                //после успешной регистрации
                    }
                    return params[0];

                case WorkWithServer.GET_TOKEN:
                    mJson = WorkWithServer.methodPOST(params[0], params[1], params[2]);
                    if (TextUtils.isEmpty(mJson)) {
                        isSuccess = false;
                        return WorkWithServer.ERR_CON;
                    }
                    mJobj = new Gson().fromJson(mJson, JsonObject.class);

                    isSuccess = mJobj.get("success").getAsBoolean();
                    if (isSuccess) {
                        User user = User.getInstance();
                        String token = mJobj.get("token").getAsString();
                        user.init(1, params[1], params[2], token);
                        user.saveData(mActivity);
                    }
                    return params[0];

                case WorkWithServer.GET_PRODUCTS:
                    mJson = WorkWithServer.methodGET(params[0]);
                    if (TextUtils.isEmpty(mJson)) {
                        isSuccess = false;
                        return WorkWithServer.ERR_CON;
                    }
                    Product[] products = new Gson().fromJson(mJson, Product[].class);

                    //заполняем статическую коллекцию продуктов, а за одно картинок,
                    // которые получили с сервера

                    MainActivity.setProducts(new ArrayList<Product>());
                    MainActivity.setBitmap(new ArrayList<Bitmap>());
                    for (Product product : products) {
                        MainActivity.getProducts().add(product);
                        InputStream is = new URL(WorkWithServer.URL_IMAGES + product.getImage())
                                .openStream();
                        MainActivity.getBitmap().add(BitmapFactory.decodeStream(is));
                    }
                    isSuccess = true;
                    return params[0];

                case WorkWithServer.GET_REVIEWS:
                    hideProgress();
                    mJson = WorkWithServer.methodGET(params[0], params[1]);
                    if (TextUtils.isEmpty(mJson))  {
                        isSuccess = false;
                        return WorkWithServer.ERR_CON;
                    }
                    mReviews = new ArrayList<Review>();
                    JsonArray jarr = new Gson().fromJson(mJson, JsonArray.class);

                    /* заполняем статическую коллекцию отзывов, которые получили с сервера
                       (немного другим способом чем продукты, но результат тотже)
                     */

                    Review review;
                    for (int i = 0; i < jarr.size(); i++) {
                        mJobj = jarr.get(i).getAsJsonObject();
                        review = new Review();
                        review.setId(mJobj.get("id").getAsInt());
                        review.setIdProduct(mJobj.get("product").getAsInt());
                        review.setRate(mJobj.get("rate").getAsInt());
                        review.setText(mJobj.get("text").getAsString());

                        review.setIdUser(mJobj.get("created_by").getAsJsonObject().get("id").getAsInt());
                        review.setUserName(mJobj.get("created_by").getAsJsonObject().get("username").getAsString());
                        mReviews.add(review);
                    }
                    return params[0];

                case WorkWithServer.POST_REVIEW:
                    mJson = WorkWithServer.methodPOST(params[0], params[1], params[2], params[3]);
                    if (TextUtils.isEmpty(mJson)) {
                        isSuccess = false;
                        return WorkWithServer.ERR_CON;
                    }
                    mJobj = new Gson().fromJson(mJson, JsonObject.class);
                    isSuccess = mJobj.get("success").getAsBoolean();
                    return params[0];
            }
        } catch (Exception ex) {
            Toast.makeText(mActivity, mActivity.getText(R.string.error), Toast.LENGTH_LONG).show();
        }
        hideProgress();
        return null;
    }

    // Завершающие действия с полученными с сервера данными. Проверяем результаты нашей обработки.

    @Override
    protected void onPostExecute(String res) {
        super.onPostExecute(res);
        switch (res){
            case WorkWithServer.REGISTRATION:
                hideProgress();
                if(isSuccess){
                    Toast.makeText(mActivity, mActivity.getText(R.string.ok_registration), Toast.LENGTH_LONG).show();
                    mActivity.setResult(LoginActivity.OK);
                    mActivity.finish(); // закрываем активность логина
                }
                else {
                    Toast.makeText(mActivity, mActivity.getText(R.string.err_sign), Toast.LENGTH_LONG).show();
                    mActivity.setResult(LoginActivity.ERR);
                }
                break;

            case WorkWithServer.GET_TOKEN:
                hideProgress();
                if(isSuccess){
                    /* т.к. этот запрос приходит с двух разных активностей, то опредиляем с какой миенно
                    и выполняем соответствующие действия.
                     */
                    if(mActivity.getClass().getName().equals(MainActivity.class.getName())) break;
                    Toast.makeText(mActivity, mActivity.getText(R.string.ok_sign), Toast.LENGTH_LONG).show();
                    mActivity.setResult(LoginActivity.OK);
                    mActivity.finish();
                }
                else {
                    Toast.makeText(mActivity, mActivity.getText(R.string.err_sign), Toast.LENGTH_LONG).show();
                    if(mActivity.getClass().getName().equals(MainActivity.class.getName())) {
                        ((MainActivity)mActivity).initMenu();
                        break;
                    }
                    mActivity.setResult(LoginActivity.ERR);
                }
                break;

            case WorkWithServer.GET_PRODUCTS:
                hideProgress();
                if(isSuccess) {
                    /* после успешного получения объектов "продуктов" с сервера, можно инициализировать
                    адаптер и прикрутить его к листвью главной активности
                     */
                    MainListAdapter adapter = new MainListAdapter(mActivity, MainActivity.getProducts());
                    ((MainActivity) mActivity).getList().setAdapter(adapter);
                }
                else Toast.makeText(mActivity, mActivity.getText(R.string.err_get_products), Toast.LENGTH_LONG).show();
                break;

            case WorkWithServer.POST_REVIEW:
                hideProgress();
                if(isSuccess)
                    Toast.makeText(mActivity, mActivity.getText(R.string.review_ok), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(mActivity, mActivity.getText(R.string.error), Toast.LENGTH_LONG).show();
                break;

            case WorkWithServer.GET_REVIEWS:
                /* После успешного получения объектов "отзывов", делаем необходимые инициализационные :)
                действия
                 */
                ProductActivity.setReviews(mReviews);
                ((ProductActivity) mActivity).init();
                break;
            case WorkWithServer.INET_OFF:
                Toast.makeText(mActivity, mActivity.getText(R.string.internet_is_off), Toast.LENGTH_LONG).show();
                break;
            case WorkWithServer.ERR_CON:
                Toast.makeText(mActivity, mActivity.getText(R.string.err_connect), Toast.LENGTH_LONG).show();
            default: Toast.makeText(mActivity, mActivity.getText(R.string.error), Toast.LENGTH_LONG).show();
        }
        hideProgress();
    }

    /**
     * Показываем ProgressDialog и ждем пока на заднем плане что-то делается
     * @param message
     * @param ctx
     */
    protected void showProgress(String message, Context ctx) {
        mProgressDialog = new ProgressDialog(ctx);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    /**
     * После завершения необходимых действий на заднем плане, закрываем ProgressDialog.
     */
    protected void hideProgress() {
        if(mProgressDialog != null) mProgressDialog.dismiss();
    }
}
