package no.app.test.taskmob;

import com.google.gson.annotations.SerializedName;
/**
 * Created by froseti on 02.05.16.
 *
 * Модель продукта (получаемого с сервера)
 */
public class Product {

    /* Аннотации для полей использованы ради интереса, :)
     посмотреть как JSON сериализируется в объект (это стороняя библиотека Gson).
     (использовал в классе AsyncOperation)*/

    @SerializedName("id")
    private int mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("img")
    private String mImage;

    @SerializedName("text")
    private String mText;

    public Product(int id, String title, String image, String text){
        mId = id;
        mTitle = title;
        mImage = image;
        mText = text;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
       mTitle = title;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
       mText = text;
    }
}
