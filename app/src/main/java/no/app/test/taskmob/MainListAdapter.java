package no.app.test.taskmob;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

/**
 * Created by froseti on 02.05.16.
 *
 * Адаптер для ListView главной активити
 */
public class MainListAdapter extends ArrayAdapter<Product> {

    public final String TAG = "MainListAdapter";
    private Activity mActivity;
    TextView tvTitle;
    ImageView ivPic;

    public MainListAdapter(Activity activity, List<Product> products) {
        super(activity, 0, products);

        mActivity = activity;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Если мы не получили представление, заполняем его
        if (convertView == null) {
            convertView = mActivity.getLayoutInflater()
                    .inflate(R.layout.item_for_main_list, null);
        }

        // Настройка представления для объекта Product
        Product c = getItem(position);
        tvTitle = (TextView) convertView.findViewById(R.id.tv_title_product);
        ivPic = (ImageView) convertView.findViewById(R.id.iv_list);

        tvTitle.setText(c.getTitle());

        //Загружаем картинки для элементов листвью
       //new DownLoadImageTask(ivPic, WorkWithServer.URL_IMAGES + c.getImage()).execute();

        ivPic.setImageBitmap(MainActivity.getBitmap().get(position));
       return convertView;
    }
}
